-- * Delay substract
--
--! @file
--!
--! @brief Input input averaging over a _g_window_ length window
--!
--! Performs an averaging of an arbitrary size input input stream over a time
--! windows of size _2**g_window_, being _g_window_ a generic input to the module.
--!
--! @section avg Averaging
--!
--! Averaging is implemented as the difference of the registered input with its
--! _2**g_window_ shifted version. Result is continuously accumulated to compute a
--! (_2**g_window_ sized) moving window over the input.
--!
--! @section div Division
--!
--! Its output is left bit shifted by _g_window_ to compute division by _2**g_window_.
--!
--! @class averager

-- * Libraries

library ieee;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

-- * Entity

--! @brief Gets g_window as a generic to compute a 2**g_window length window.
--!
--! @details Rst and clk have standard functionality\n
--! Samples is an arbitrary number of bites input data bus\n\n
--! Average is the sum of all input in the window, divided by its length,
--! thus the average\n
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...
--!
entity delay_substract is
  generic (g_delay : natural);
  port (
    --!\name Global signals
    --!\{
    clk    : in  std_logic;
    rst    : in  std_logic;
    --!\}
    --!\name Input and averaged input
    --!\{
    input  : in  signed;
    --! length is input’lentgh+1
    output : out signed
   --!\}
    );
end entity delay_substract;

-- * Architecture

--! @brief Implements a simple, general purpose moving averaging.
--!
--! @details It delays the incoming sampling signals, sampling them in parallel.\n
--! Then, takes the difference of the two parallel streams\n
--! accumulating the difference.\n
--! Finally, redimensions the results to compute the division by the window lenght
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...
--!
architecture modular of delay_substract is

  -- ** types

  --! Shift register for the incoming input. Its length is given by the window
  type input_array is array (g_delay-1 downto 0) of signed(input'range);

  -- ** signals
  --
  --! Registered incoming input
  signal input_registered : signed(input'range) := (others => '0');

  --! Shifted incoming input
  signal input_shifted : signed(input'range) := (others => '0');

  --! Signed registered input
  signal s_input_registered : signed(input'length downto 0) := (others => '0');

  --! Signed shifted input
  signal s_input_shifted : signed(input'length downto 0) := (others => '0');

  --! Internal output
  signal diff : signed(input'length downto 0) := (others => '0');

  --! Shift register
  signal input_srl : input_array := (others => (others => '0'));

begin

  -- ** register input

  --! @brief Register process
  --!
  --! @details Just register incoming input\n
  --! The register size it’s been fixed during signal declaration\n
  --! Note that it performs signed arithmetic.
  --!
  --! @section sec1 Section1
  --!
  --! All is clear, I guess ...
  --!
  --! @section sec2 Section2
  --!
  --! Don’t know what to say ...

  p_mw_register_input : process (clk) is
  begin
    if rising_edge(clk) then
      if rst = '1' then
        input_registered <= (others => '0');
      else
        input_registered <= input;
      end if;
    end if;
  end process;

  -- ** shift input by 2**g_window=c_window_length clock periods

  --! @brief Shift register process
  --!
  --! @details Sum up the result of the difference block\n
  --! The register size it’s been fixed during signal declaration\n
  --! Note that it performs signed arithmetic.
  --!
  --! @section sec1 Section1
  --!
  --! All is clear, I guess ...
  --!
  --! @section sec2 Section2
  --!
  --! Don’t know what to say ...

  p_mw_shift_register : process (clk) is
  begin
    if rising_edge(clk) then
      -- shift register mechanism
      input_srl(input_srl'high downto 1) <= input_srl(input_srl'high-1 downto 0);
      if rst = '1' then
        input_srl(input_srl'low) <= (others => '0');
        input_shifted            <= (others => '0');
      else
        -- shift register input
        input_srl(input_srl'low) <= input_registered;
        -- shift register output
        input_shifted            <= input_srl(input_srl'high);
      end if;
    end if;
  end process;

  -- ** resize

  s_input_registered <= resize(input_registered, s_input_registered'length);
  s_input_shifted    <= resize(input_shifted, s_input_shifted'length);

  -- ** take difference of registered and shifted input

  --! @brief Difference process
  --!
  --! @details Computes the difference of delayed and registerd input\n
  --! The register size it’s been fixed during signal declaration\n
  --! Note that it performs signed arithmetic.
  --!
  --! @section sec1 Section1
  --!
  --! All is clear, I guess ...
  --!
  --! @section sec2 Section2
  --!
  --! Don’t know what to say ...

  p_mw_difference : process (clk) is
  begin
    if rising_edge(clk) then
      if rst = '1' then
        diff <= (others => '0');
      else
        diff <= s_input_registered - s_input_shifted;
      end if;
    end if;
  end process;

  --! output size is fixed, that of diff
  output <= diff;

end architecture modular;
