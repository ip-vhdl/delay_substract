Download

#+begin_src sh
  gitlab.com/csantosb/eda-common
#+end_src

then do a (after having correctly setup the appropriate paths)

#+begin_src sh
  ./setup.sh
#+end_src

which will copy

#+begin_src sh
  gitlab.com/csantosb/eda-common/Makefile.edacommon
#+end_src

here, and will create necessary symbolic links.

Then just adjust variables in =Makefile= and

#+begin_src sh
  make run
#+end_src

For cleaning up results, and restarting from scratch do a

#+begin_src sh
  make clean
#+end_src
